from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string

class Command(BaseCommand):
    help = 'Generate Sample Template'

    def add_arguments(self, parser):
        parser.add_argument('output', type=str)

    def handle(self, *args, **options):
        with open(options['output'], 'w') as fp:
            fp.write(render_to_string("home.html", {}))
            self.stdout.write(self.style.SUCCESS("Generated sample to {}".format(options['output'])))
