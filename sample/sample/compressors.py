
from compressor.base import Compressor
from django.conf import settings
import os

# Some constants for nicer handling.
SOURCE_HUNK, SOURCE_FILE = 'inline', 'file'
METHOD_INPUT, METHOD_OUTPUT = 'input', 'output'

class MyCompressor(Compressor):
    def get_filepath(self, content, basename=None):
        parts = []
        if basename:
            filename = os.path.split(basename)[1]
            parts.append(os.path.splitext(filename)[0])
        parts.extend([self.resource_kind])
        return os.path.join(self.output_dir, self.output_prefix, '.'.join(parts))


class CssCompressor(MyCompressor):

    output_mimetypes = {'text/css'}

    def split_contents(self):
        if self.split_content:
            return self.split_content
        self.media_nodes = []
        for elem in self.parser.css_elems():
            data = None
            elem_name = self.parser.elem_name(elem)
            elem_attribs = self.parser.elem_attribs(elem)
            if elem_name == 'link' and 'rel' in elem_attribs and elem_attribs['rel'].lower() == 'stylesheet':
                basename = self.get_basename(elem_attribs['href'])
                filename = self.get_filename(basename)
                data = (SOURCE_FILE, filename, basename, elem)
            elif elem_name == 'style':
                data = (SOURCE_HUNK, self.parser.elem_content(elem), None, elem)
            if data:
                self.split_content.append(data)
                media = elem_attribs.get('media', None)
                # Append to the previous node if it had the same media type
                append_to_previous = self.media_nodes and self.media_nodes[-1][0] == media
                # and we are not just precompiling, otherwise create a new node.
                if append_to_previous and settings.COMPRESS_ENABLED:
                    self.media_nodes[-1][1].split_content.append(data)
                else:
                    node = self.copy(content=self.parser.elem_str(elem))
                    node.split_content.append(data)
                    self.media_nodes.append((media, node))
        return self.split_content

    def output(self, *args, **kwargs):
        if (settings.COMPRESS_ENABLED or settings.COMPRESS_PRECOMPILERS
                or kwargs.get('forced', False)):
            # Populate self.split_content
            self.split_contents()
            if hasattr(self, 'media_nodes'):
                ret = []
                for media, subnode in self.media_nodes:
                    subnode.extra_context.update({'media': media})
                    ret.append(subnode.output(*args, **kwargs))
                return ''.join(ret)
        return super().output(*args, **kwargs)


class JsCompressor(MyCompressor):

    output_mimetypes = {'text/javascript'}

    def split_contents(self):
        if self.split_content:
            return self.split_content
        self.extra_nodes = []
        for elem in self.parser.js_elems():
            attribs = self.parser.elem_attribs(elem)
            if 'src' in attribs:
                basename = self.get_basename(attribs['src'])
                filename = self.get_filename(basename)
                content = (SOURCE_FILE, filename, basename, elem)
            else:
                content = (SOURCE_HUNK, self.parser.elem_content(elem), None, elem)
            self.split_content.append(content)
            if 'async' in attribs:
                extra = ' async'
            elif 'defer' in attribs:
                extra = ' defer'
            else:
                extra = ''
            # Append to the previous node if it had the same attribute
            append_to_previous = (self.extra_nodes
                                  and self.extra_nodes[-1][0] == extra)
            if append_to_previous and settings.COMPRESS_ENABLED:
                self.extra_nodes[-1][1].split_content.append(content)
            else:
                node = self.copy(content=self.parser.elem_str(elem))
                node.split_content.append(content)
                self.extra_nodes.append((extra, node))
        return self.split_content

    def output(self, *args, **kwargs):
        if (settings.COMPRESS_ENABLED or settings.COMPRESS_PRECOMPILERS
                or kwargs.get('forced', False)):
            self.split_contents()
            if hasattr(self, 'extra_nodes'):
                ret = []
                for extra, subnode in self.extra_nodes:
                    subnode.extra_context.update({'extra': extra})
                    ret.append(subnode.output(*args, **kwargs))
                return '\n'.join(ret)
        return super().output(*args, **kwargs)

    def filter_input(self, forced=False):
        """
        Passes each hunk (file or code) to the 'input' methods
        of the compressor filters.
        """
        content = []
        for hunk in self.hunks(forced):
            # If a file ends with a function call, say, console.log()
            # but doesn't have a semicolon, and the next file starts with
            # a (, the individual files are ok, but when combined you get an
            # error like TypeError...
            # Forcing a semicolon in between fixes it.
            if settings.COMPRESS_ENABLED or forced:
                hunk += ";"
            content.append(hunk)
        return content
